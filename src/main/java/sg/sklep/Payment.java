package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class Payment extends HttpServlet {
    private static final String PATH_VIEW_INDEX  = "payment/index.jsp";
    private static final String PATH_VIEW_SUCCESS  = "payment/success.jsp";
    private static final String NAME_CARD_NO = "cardNo";
    private static final String NAME_CCV_CODE = "ccvCode";
    private static final String NAME_VALID_UNTIL_MONTH = "validUntilMonth";
    private static final String NAME_VALID_UNTIL_YEAR = "validUntilYear";
    private static final String NAME_CARDS_LIST = "cardsList";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PATH_VIEW_INDEX).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String cardNo = req.getParameter(NAME_CARD_NO);
        String ccvCode = req.getParameter(NAME_CCV_CODE);
        String validUntilMonth = req.getParameter(NAME_VALID_UNTIL_MONTH);
        String validUntilYear = req.getParameter(NAME_VALID_UNTIL_YEAR);

        // Reading data from the session object
        ArrayList<String> cardsList = (ArrayList<String>) req.getSession().getAttribute(NAME_CARDS_LIST);
        if (cardsList == null) {
            cardsList = new ArrayList<>();
        }

        if (isValid(cardNo, ccvCode, validUntilMonth, validUntilYear)) {
            cardsList.add(cardNo);

            req.setAttribute(NAME_CARDS_LIST, cardsList);
            req.getRequestDispatcher(PATH_VIEW_SUCCESS).forward(req, resp);

            // Saving updated data back to the session object
            req.getSession().setAttribute(NAME_CARDS_LIST, cardsList);
        } else {
            resp.setStatus(400);
            req.getRequestDispatcher(PATH_VIEW_INDEX).forward(req, resp);
        }
    }

    private boolean isValid(String cardNo, String ccvCode, String month, String year) {
        if (!isValidCardNo(cardNo)) return false;

        if (!isValidCcvCode(ccvCode)) return false;

        if (!isValidMonth(month)) return false;

        if (!isValidYear(year)) return false;

        return true;
    }

    private boolean isValidMonth(String month) {
        return month != null && month.matches("(0[1-9])|([1-9])|(1[0-2])");
    }

    private boolean isValidYear(String year) {
        return year != null && year.matches("([0-9])|([0-9][0-9])");
    }

    private static final int digitsNoCcvCode = 3;

    private boolean isValidCcvCode(String ccvCode) {
        if (ccvCode == null) return false;

        if (ccvCode.length() != digitsNoCcvCode) return false;

        if (!ccvCode.matches("[0-9]*")) return false;

        return true;
    }

    private static final int digitsNoCardNo = 16;

    private boolean isValidCardNo(String cardNo) {
        if (cardNo == null) return false;

        if (cardNo.length() != digitsNoCardNo) return false;

        if (!cardNo.matches("[0-9]*")) return false;

        return true;
    }
}
