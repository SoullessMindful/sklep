<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page isELIgnored="false" %>

<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Płatności</title>
  <link rel="stylesheet" href="payment/payment.css">
</head>
<body>
  <main>
    <h2 class="h-payment">
      Gratulujemy zakupu! Podaj dane do płatności, aby zakończyć transakcję.
    </h2>
    <form method="POST" action="payment">
      <div class="block-inputs">
        <div>
          <div>
            <label for="inp-card-no">Numer Karty</label>
          </div>
          <input type="text" name="cardNo" id="inp-card-no" value="${param.cardNo}" />
        </div>
        <div>
          <div>
            <label for="inp-valid-until-month">Ważna do</label>
          </div>
          <div>
            <input type="text" name="validUntilMonth" id="inp-valid-until-month" class="txt-date" />
            <input type="text" name="validUntilYear" id="inp-valid-until-year" class="txt-date" />
          </div>
        </div>
        <div>
          <div>
            <label for="inp-ccv-code">Kod CCV</label>
          </div>
          <input type="text" name="ccvCode" id="inp-ccv-code" class="txt-ccv" />
        </div>
       </div>
       <div class="block-buttons">
         <input type="submit" value="Zapłać" />
       </div>
    </form>
  </main>
</body>
</html>