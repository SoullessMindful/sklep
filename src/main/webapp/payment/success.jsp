<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="java.util.ArrayList"%>
<%@ page isELIgnored="false" %>

<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Płatności</title>
  <link rel="stylesheet" href="payment/payment.css">
</head>
<body>
  <main>
    <h2 class="h-payment">
      Twoja płatność została zarejestrowana.
    </h2>
    <p>
      Karta o numerze ${param.cardNo} zostanie obciążona.
    </p>
    <p>
      Dotychczasowe numery kart:
    </p>
    <ul>
      <%
        ArrayList<String> cardsList = (ArrayList<String>) request.getAttribute("cardsList");
        for (String card : cardsList) {
          out.println(
            "<li>" +
            card +
            "</li>"
          );
        }
      %>
    </ul>
  </main>
</body>
</html>